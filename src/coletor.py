# -*- coding: utf-8 -*-
'''
import csv
with open('piabeta.csv', 'rb') as csvfile:
  spamreader = csv.reader(csvfile, delimiter=';', quotechar="\'")
  lines=[line for line in spamreader]
  colunas=zip(*lines[1:200])
'''
from openpyxl import load_workbook as lw

def analisa(colunas):
    #print lines[0:4]
    a = [c.value for c in colunas[18]]
    #print colunas[17]
    mx = max(a)
    pa = a.index(mx)
    print("max:",mx, pa)
    vlr = [l.value for l in colunas[17] if l.value != None][1:]
    print (vlr [:8])
    vl = [float(l) for l in vlr]
    vl = sum(vl[:pa])
    print "-----------------------"
    print mx, pa, vl
    #print colunas[1][:100]

def main():
    wb = lw(filename = r'../data/EnxurradaPiabeta.xlsx')
    sh = wb.get_sheet_names
    s08 = wb.get_sheet_by_name('2008')
    analisa(s08.columns)

if __name__ == '__main__':
    main()

